package br.com.itau.creditcard.services;

import br.com.itau.creditcard.customerClient.Customer;
import br.com.itau.creditcard.customerClient.CustomerClient;
import br.com.itau.creditcard.exceptions.CreditCardNotFoundException;
import br.com.itau.creditcard.models.CreditCard;
import br.com.itau.creditcard.repositories.CreditCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CreditCardService {

    @Autowired
    private CustomerClient customerClient;

    @Autowired
    private CreditCardRepository creditCardRepository;

    public CreditCard create(CreditCard creditCard) {
        Customer customer = customerClient.getById(creditCard.getCustomer());
        creditCard.setCustomer(customer.getId());

        creditCard.setActive(false);

        return creditCardRepository.save(creditCard);
    }

    public CreditCard update(CreditCard updatedCreditCard) {
        CreditCard creditCard = getById(updatedCreditCard.getId());

        creditCard.setActive(updatedCreditCard.getActive());

        return creditCardRepository.save(creditCard);
    }

    public CreditCard getById(Long id) {
        Optional<CreditCard> byId = creditCardRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CreditCardNotFoundException();
        }

        return byId.get();
    }




}

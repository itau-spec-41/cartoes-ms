package br.com.itau.creditcard.repositories;

import br.com.itau.creditcard.models.CreditCard;
import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, Long> {
}

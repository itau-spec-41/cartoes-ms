package br.com.itau.creditcard.customerClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CUSTOMER")
public interface CustomerClient {

    @GetMapping("/{id}")
    Customer getById(@PathVariable Long id);

}
